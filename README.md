# Nginx Proxy Manager

## Default

Email: ```admin@example.com```
Password: ```changeme```


## Reset Password
```
docker exec -it <container-name> sh
```
```
apt update && apt install sqlite3 -y
```
```
sqlite3 /data/database.sqlite
```

```
UPDATE user SET is_deleted=1;
```
```
.exit
```
```
exit
```
Jika container NPM Anda telah berjalan, mulai ulang. Jika belum berjalan, mulai sekarang.
```
docker restart <container-name>
```

Kemudian atur semua pengguna agar tidak terhapus dengan menjalankan:
```
docker exec -it <container-name> sh
sqlite3 /data/database.sqlite
UPDATE user SET is_deleted=0;
.exit
exit
```
